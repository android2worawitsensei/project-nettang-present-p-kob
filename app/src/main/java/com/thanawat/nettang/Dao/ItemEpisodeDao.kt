package com.thanawat.nettang.Dao

import androidx.room.*
import com.thanawat.nettang.Entity.ItemEpisode
import kotlinx.coroutines.flow.Flow


@Dao
interface ItemEpisodeDao {
    @Insert
    suspend fun insert(item : ItemEpisode)

    @Update
    suspend fun update(item: ItemEpisode)

    @Delete
    suspend fun delete(item: ItemEpisode)

    @Query("SELECT * from ItemEpisode")
    fun getItem(): Flow<List<ItemEpisode>>
}
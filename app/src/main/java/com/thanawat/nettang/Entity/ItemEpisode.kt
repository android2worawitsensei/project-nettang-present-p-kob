package com.thanawat.nettang.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ItemEpisode(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "Draw")
    val itemEpisodeDraw: Int,
    @ColumnInfo(name = "name")
    val itemEpisodeName: String,
    @ColumnInfo(name = "Description")
    val itemEpisodeDescrip: String
)
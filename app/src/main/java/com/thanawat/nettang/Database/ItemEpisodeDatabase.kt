package com.thanawat.nettang.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.thanawat.nettang.Dao.ItemEpisodeDao
import com.thanawat.nettang.Entity.ItemEpisode

@Database(entities = [ItemEpisode::class], version = 1, exportSchema = false)
abstract class ItemEpisodeDatabase : RoomDatabase() {

    abstract fun itemEpisodeDao() : ItemEpisodeDao

    companion object{
        @Volatile
        private var INSTANCE: ItemEpisodeDatabase? = null
        fun getDatabase(context: Context): ItemEpisodeDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ItemEpisodeDatabase::class.java,
                    "item_Episode_database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

}